import React from 'react';
import {Button, FormControl, FormGroup, PageHeader} from "react-bootstrap";
import { connect } from "react-redux";
import {fetchDecode, fetchEncode, onChangeDecode, onChangeEncode, onChangePass} from "../../store/actions";

class Form extends React.Component {

    getValidationState() {
        const length = this.props.pass.length;
        if (length === 0) return 'error';
    }

    render() {

    const state = {
            pass: this.props.pass,
            decode: this.props.decode,
            encode: this.props.encode
        };

    const onSubmitDecode = (e) => {
            e.preventDefault();
            this.props.fetchResponseDecode(state);
        };
    const onSubmitEncode = (e) => {
            e.preventDefault();
            this.props.fetchResponseEncode(state);
        };

    const wellStyles = { maxWidth: 400, margin: '10px auto 10px' };

        return (
            <form>
                <FormGroup
                    controlId="formBasicText"
                    validationState={this.getValidationState()}
                >
                    <FormControl
                        name='decode'
                        style={wellStyles}
                        type="text"
                        value={this.props.decode}
                        placeholder="Encode text"
                        onChange={(e) => this.props.onChangeDecode(e.target.value)}
                    />
                    <PageHeader style={{textAlign: 'center'}}>
                        <small>Encode key</small>
                        <FormControl.Feedback />
                    </PageHeader>
                    <FormControl
                        name='pass'
                        style={wellStyles}
                        type="text"
                        value={this.props.pass}
                        placeholder="Encode key"
                        onChange={(e) => this.props.onChangePass(e.target.value)}
                    />

                    <div className="well" style={wellStyles}>
                        <Button
                            onClick={onSubmitEncode}
                            disabled={this.props.pass.length === 0}
                            type='button' bsSize="small" bsStyle="success" block>
                            Decode text
                        </Button>
                        <Button
                            onClick={onSubmitDecode}
                            disabled={this.props.pass.length === 0}
                            type='button' bsStyle="info" bsSize="small" block>
                            Encode text
                        </Button>
                    </div>
                    <PageHeader style={{textAlign: 'center'}}>
                        <small>Decode text</small>
                        <FormControl.Feedback />
                    </PageHeader>
                    <FormControl
                        name='encode'
                        style={wellStyles}
                        type="text"
                        value={this.props.encode}
                        placeholder="Text for decode"
                        onChange={(e) => this.props.onChangeEncode(e.target.value)}
                    />
                </FormGroup>
            </form>
        );
    };
}
const mapStateToProps = state => {
    return {
        encode: state.encode,
        decode: state.decode,
        pass: state.pass
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onChangeDecode: data => dispatch(onChangeDecode(data)),
        onChangeEncode: data => dispatch(onChangeEncode(data)),
        onChangePass: data => dispatch(onChangePass(data)),
        fetchResponseDecode: data => dispatch(fetchDecode(data)),
        fetchResponseEncode: data => dispatch(fetchEncode(data)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);