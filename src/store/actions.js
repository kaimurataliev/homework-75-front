import axios from '../axios';
export const CHANGE_DECODE = 'CHANGE_DECODE';
export const CHANGE_ENCODE = 'CHANGE_ENCODE';
export const CHANGE_PASS = 'CHANGE_PASS';

export const onChangeDecode = (code) => {
  return {type: CHANGE_DECODE, code}
};

export const onChangeEncode = (code) => {
    return {type: CHANGE_ENCODE, code}
};

export const onChangePass = (code) => {
    return {type: CHANGE_PASS, code}
};

export const fetchDecode = (state) => {
    return dispatch => {
        axios.post('/decode', state)
            .then(response => {
                dispatch(onChangeEncode(response.data))
            })
    }
};

export const fetchEncode = (state) => {
    return dispatch => {
        axios.post('/encode', state)
            .then(response => {
                dispatch(onChangeDecode(response.data))
            })
    }
};
