import * as actions from './actions';

const initialState = {
    decode: '',
    encode: '',
    pass: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
      case actions.CHANGE_DECODE:
          return {...state, decode: action.code};
      case actions.CHANGE_ENCODE:
          return {...state, encode: action.code};
      case actions.CHANGE_PASS:
          return {...state, pass: action.code};
      default:
          return state;
  }
};

export default reducer;